## What is this module for ?
This module computes the heating, cooling and electrical power demand of buildings, based on :
- A geometry file created by the geometry module, describing each level of the buildings, as well as the sky view map of each surface (walls and roofs).
- Hypothesis for the dynamic thermal simulation for each use and building : standard (RT2012 prodiles and hypothesis + typical values) or custom (same standard files with custom parameters values).



## Installation instructions
You need to install python libraries before using the module :
```
pip install -r /path/to/module/python/requirements.txt
``` 

## Use
```
run.py -g <geometry_file_path> -s <std_hyps_file_path> -o <output_file_path>
``` 

Optional weather parameter:
```
-w
```
Accepts EPW file, CSV file or RT2012 zone 
Default RT2012 for Paris region