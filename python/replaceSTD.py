# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 12:08:22 2019

@author: G.Peronato
"""

import os
import pandas as pd
import getopt
import sys
import numpy as np

def do(climelioth, STDpath):

    power = climelioth.copy()
    climelioth_variables = power.columns

    dfs = []

    for file in os.listdir(STDpath):

        STD = pd.read_csv(os.path.join(STDpath,file))

        chunks = file.replace(".csv","").split("_")
        building_id = "_".join(chunks[0:-1])
        use = chunks[-1]
        replacement_variables = STD.columns

        if building_id in power.building_id.unique():
        
            for var in replacement_variables:
                if var in climelioth_variables:

                    power_ts_rep = STD[var].values

                    # Force the heat/cool recovery not to exceed the heating power
                    if var == "p_heat_reco":
                        power_ts_rep = np.where(power_ts_rep > STD["p_heat"].values, STD["p_heat"].values, power_ts_rep)

                    if var == "p_cool_reco":
                        power_ts_rep = np.where(power_ts_rep > STD["p_cool"].values, STD["p_cool"].values, power_ts_rep)

                    # Make the cooling power negative to follow the climelioth/smartgrid convention
                    if var in ["p_cool", "p_cool_reco"]:
                        power_ts_rep *= -1.0

                    print("Replacing power time series - building: {} - use: {} - variable: {}".format(building_id, use, var))
                    print("Energy (climelioth): {} - Energy (replacement): {}".format(str(power.loc[(power.use == use) & (power.building_id == building_id), var].values.sum()), str(power_ts_rep.sum())))

                    power.loc[(power.use == use) & (power.building_id == building_id), var] = power_ts_rep
                
    return power

def main(argv):
    # Find the path of the script
    script_path = os.path.abspath(__file__)
    dir_path = os.path.dirname(script_path)
    sys.path.append(dir_path + "/python/")
    
    
    STDpath = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\STD\in\1"
    climeliothpath = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\climelioth\out\11b140de-ffbc-11e9-bf6e-d4e099056fc8\climelioth.parquet"
    outputpath = "test.parquet"
    
    try:
        opts, args = getopt.getopt(argv, "s:c:o:", ["STDpath","climelioth=","output="])
    except getopt.GetoptError:
        print('run.py -s <STD> -c <climelioth> -o <output_file_path>')
        sys.exit(2)
     
    for opt, arg in opts:
        if opt in ("-c", "--climelioth"):
            climeliothpath = arg
        if opt in ("-s", "--STD"):
            STDpath = arg
        elif opt in ("-o", "--output"):
            outputpath = arg
    
    climelioth = pd.read_parquet(climeliothpath)
    power = do(climelioth,STDpath)
    power.to_parquet(outputpath)
    
    return climelioth, power


if __name__ == "__main__":  
	climelioth, power = main(sys.argv[1:])

