import numpy as np
from scipy.optimize import fsolve
from .face import TransparentFace, OpaqueFace
from .constants import constants
import warnings
from .internal_gain import LightingGain
from .airflow import HygienicAirflow
import warnings
warnings.filterwarnings('ignore', 'The iteration is not making good progress')

class Group:

    def __init__(self, floor_area, volume, internal_area, mass_area_ratio, mass_heat_capacity):
        self.floor_area = floor_area
        self.volume = volume
        self.internal_area = internal_area

        # if mass_area_ratio > internal_area/floor_area:
        #     warnings.warn('Mass area ratio exceeds internal to floor area ratio, its value was forced to : '+ str(internal_area/floor_area))
        #     mass_area_ratio = internal_area/floor_area

        self.mass_area_ratio = mass_area_ratio

        self.c_m = mass_heat_capacity*floor_area
        self.a_m = mass_area_ratio*floor_area

        self.opaque_faces = []
        self.transparent_faces = []
        self.airflows = []
        self.internal_gains = []
        self.natural_ventilation = False
        self.heating = False
        self.cooling = False

        self.heating_system_conv_p_frac = 0.5
        self.cooling_system_conv_p_frac = 0.5


    def add_heating_system(self, system_type, profile, max_heating_power = 1e9):
        self.heating = system_type != "none"
        self.heating_system_profile = profile
        self.max_heating_power = max_heating_power

        # From RT2012 p. 512
        conv_frac = {
            'none': 0.0,
            'convective': 0.95,
            'radiators': 0.7,
            'radiant_floor': 0.5,
            'radiant_ceiling': 0.2,
            'radiant_wall': 0.35
        }

        if system_type in conv_frac.keys():
            self.heating_system_conv_p_frac = conv_frac[system_type]
        else:
            raise ValueError('Heating system type' + system_type +  ' is not available (should be one of : ' + ', '.join(conv_frac.keys()) + ')')
        

    def add_cooling_system(self, system_type, profile, max_cooling_power = 1e9):
        self.cooling = system_type != "none"
        self.cooling_system_profile = profile
        self.max_cooling_power = max_cooling_power

        # From RT2012 p. 512
        conv_frac = {
            'none': 0.0,
            'convective': 0.95,
            'radiant_beam': 0.8,
            'radiant_ceiling': 0.5,
            'radiant_wall': 0.35,
            'radiant_floor': 0.2
        }

        if system_type in conv_frac.keys():
            self.cooling_system_conv_p_frac = conv_frac[system_type]
        else:
             raise ValueError('Cooling system type' + str(system_type) +  ' is not available (should be one of : ' + ', '.join(conv_frac.keys()) + ')')


    def set_temperature_setpoints(self, heating_setpoint, cooling_setpoint):
        self.heating_setpoint = heating_setpoint
        self.cooling_setpoint = cooling_setpoint

    def set_natural_ventilation(self, profile, max_acr, ext_temp_lower_limit=16+273.15, opening_ratio=0):
        self.natural_ventilation = True
        self.natural_ventilation_profile = profile
        self.natural_ventilation_max_acr = max_acr
        self.natural_ventilation_ext_temp_lower_limit = ext_temp_lower_limit

        tf_area = 0
        for tf in self.transparent_faces:
            tf_area += tf.area
        self.natural_ventilation_opening_area = opening_ratio*tf_area


    def add_faces(self, faces):
        for f in faces:
            if isinstance(f, OpaqueFace):
                self.opaque_faces.append(f)
            elif isinstance(f, TransparentFace):
                self.transparent_faces.append(f)
            else:
                raise ValueError('')

        # Limit the mass area ratio based on window area
        # window_area = 0
        # for f in self.transparent_faces:
        #     window_area += f.area
        # if window_area > 0:
        #     max_mass_area_ratio = (self.internal_area - window_area)/self.floor_area
        #     if max_mass_area_ratio < self.mass_area_ratio:
        #         warnings.warn('Mass area ratio is too large given the window area, its value was forced to : ' + str(max_mass_area_ratio))
        #         self.mass_area_ratio = max_mass_area_ratio
        #         self.a_m = max_mass_area_ratio*self.floor_area

    def add_airflows(self, airflows):
        for a in airflows:
            self.airflows.append(a)
            if isinstance(a, HygienicAirflow):
                self.hygienic_mass_flowrate = a.mass_flowrate

    def add_internal_gains(self, internal_gains):
        for ia in internal_gains:
            self.internal_gains.append(ia)

    def compute_faces_radiation(self, context):
        for f in faces:
            f.compute_ext_radiation_flows(context)

    def compute_group_model(self, context):

        self.hg_ei = self.m_eq*(constants.c_a + constants.c_v*self.w_eq)

        hg_ms = constants.h_is*self.a_m

        h_th = 0
        for of in self.opaque_faces:
            of.compute_heat_loss_coeff()
            h_th += of.h_th

        hg_em = 1/(1/h_th - 1/hg_ms)

        window_area = 0
        hg_es = 0
        for tf in self.transparent_faces:
            tf.compute_heat_loss_coeff()
            hg_es += tf.h_th
            window_area += tf.area


        a_tgroup = self.internal_area #4.5*self.floor_area # # 
        hg_is = a_tgroup*(1/constants.h_ci - 1/constants.h_is)

        # Radiation coefficients
        f_rs = (a_tgroup - self.a_m - hg_es/constants.h_is)/a_tgroup
        f_rm = self.a_m/a_tgroup
        f_rsd = (a_tgroup - self.a_m - window_area)/(a_tgroup - window_area)
        f_rmd = self.a_m/(a_tgroup - window_area)

        # Temperature sensor convective/radiant sensitivity
        p_top = 0.5*(1+constants.h_ci/(1.2*constants.h_ri))
        p_trm = 1+constants.h_ci/(1.2*constants.h_ri)

        # Group matrix
        F = np.zeros((4, 4))
        
        F[0, 0] = hg_is
        F[0, 1] = -hg_is

        F[1, 0] = -hg_is
        F[1, 1] = hg_is + hg_es + hg_ms
        F[1, 2] = -hg_ms

        F[2, 1] = -hg_ms
        F[2, 2] =  self.c_m/context.delta_t + 0.5*(hg_em + hg_ms)

        p_sd = 0.5
        F[3, 0] = p_sd + (1-p_sd)*(1-p_trm)
        F[3, 1] = (1-p_sd)*p_trm

        # Group matrix for heating
        F_heating = F.copy()
        F_heating[0, 3] = -self.heating_system_conv_p_frac
        F_heating[1, 3] = -f_rs*(1-self.heating_system_conv_p_frac)
        F_heating[2, 3] = -f_rm*(1-self.heating_system_conv_p_frac)

        p_sd = self.heating_system_conv_p_frac
        F_heating[3, 0] = 1.0
        F_heating[3, 1] = 0.0

        # Group matrix for cooling
        F_cooling = F.copy()
        F_cooling[0, 3] = -self.heating_system_conv_p_frac
        F_cooling[1, 3] = -f_rs*(1-self.cooling_system_conv_p_frac)
        F_cooling[2, 3] = -f_rm*(1-self.cooling_system_conv_p_frac)

        p_sd = self.cooling_system_conv_p_frac
        F_cooling[3, 0] = 1.0
        F_cooling[3, 1] = 0.0

        # Heat balance equation RHS terms
        B0 = np.zeros(4)
        B0[2] = self.c_m/context.delta_t - 0.5*(hg_em + hg_ms)

        B1 = np.zeros(4)
        B1[1] = hg_es
        B1[2] = hg_em

        self.F_heating = F_heating
        self.F_cooling = F_cooling
        self.B0 = B0
        self.B1 = B1

        self.hg_is = hg_is
        self.hg_em = hg_em
        self.hg_es = hg_es
        self.hg_ms = hg_ms

        self.f_rs = f_rs
        self.f_rm = f_rm
        self.f_rsd = f_rsd
        self.f_rmd = f_rmd



    def compute_equivalent_airflow(self):
        m_eq = 0
        w_eq = 0
        h_eq = 0
        for a in self.airflows:
            m_eq += a.mass_flowrate
            w_eq += a.mass_flowrate*a.humidity
            h_eq += a.mass_flowrate*((constants.c_a + constants.c_v*a.humidity)*a.temperature + constants.h_fg*a.humidity)

        self.m_eq = m_eq
        self.h_eq = np.zeros(len(m_eq))
        self.w_eq = np.zeros(len(m_eq))
        self.h_eq[m_eq > 0] = h_eq[m_eq > 0]/m_eq[m_eq > 0]
        self.w_eq[m_eq > 0] = w_eq[m_eq > 0]/m_eq[m_eq > 0]


    def compute_ext_radiation_flows(self, context):

        for tf in self.transparent_faces:
            tf.compute_ext_radiation_flows(context)

        for of in self.opaque_faces:
            of.compute_ext_radiation_flows(context)


    def compute_equivalent_temperatures(self, context):

        # Equivalent air temperature entering group from exterior and other groups
        theta_eieq = (self.h_eq - constants.h_fg*self.w_eq)/(constants.c_a + constants.c_v*self.w_eq)

        phi_sl = 0
        for tf in self.transparent_faces:
            phi_sl += tf.f_s2 + tf.f_tvc

        if self.hg_es > 0:
            theta_es = context.air_temperature + phi_sl/self.hg_es
        else:
            theta_es = context.air_temperature

        # Equivalent exterior opaque surfaces temperature
        phi_sh = 0
        for of in self.opaque_faces:
            phi_sh += of.phi_sh_s + of.phi_sh_vc + of.phi_sl_s + of.phi_sl_vc

        theta_em = context.air_temperature + phi_sh*(1/self.hg_em + 1/self.hg_ms)

        self.theta_e = np.array([theta_eieq, theta_es, theta_em, np.zeros(len(theta_es))]).T

    def compute_equivalent_temperatures_at_timestep(self, context, i, theta_i):

        m_eq = 0
        w_eq = 0
        h_eq = 0

        for a in self.airflows:
            m_eq += a.mass_flowrate[i]
            w_eq += a.mass_flowrate[i]*a.humidity[i]
            h_eq += a.mass_flowrate[i]*((constants.c_a + constants.c_v*a.humidity[i])*a.temperature[i] + constants.h_fg*a.humidity[i])

        m_eq = m_eq

        if m_eq > 0:
            h_eq = h_eq/m_eq
            w_eq = w_eq/m_eq
            theta_eieq = (h_eq - constants.h_fg*w_eq)/(constants.c_a + constants.c_v*w_eq)
        else:
            theta_eieq = 0.0

        theta_es = self.theta_e[i, 1]
        theta_em = self.theta_e[i, 2]

        return np.array([theta_eieq, theta_es, theta_em, 0])

    def compute_internal_gains(self, context):

        f_s1 = np.zeros(len(context.air_temperature))
        f_s3 = np.zeros(len(context.air_temperature))
        for tf in self.transparent_faces:
            f_s1 += tf.f_s1
            f_s3 += tf.f_s3

        
        # Repartition of internal gains between a convective and radiative part
        phi_conv = 0
        phi_rad = 0
        for ig in self.internal_gains:
            if isinstance(ig, LightingGain):
                phi_solar = f_s1/self.floor_area
                ig.modulate(phi_solar)

            phi_conv += ig.power*ig.convective_fraction
            phi_rad += ig.power*(1-ig.convective_fraction)

        phi_i = constants.f_sa*f_s1 + f_s3 + phi_conv
        phi_s = self.f_rsd*(1-constants.f_sa)*f_s1 + self.f_rs*phi_rad
        phi_m = self.f_rmd*(1-constants.f_sa)*f_s1 + self.f_rm*phi_rad

        self.phi_int = np.array([phi_i, phi_s, phi_m, np.zeros(len(phi_i))]).T


    def simulate(self, context):

        N = len(context.air_temperature)

        self.compute_equivalent_airflow()
        self.compute_group_model(context)
        self.compute_ext_radiation_flows(context)
        self.compute_equivalent_temperatures(context)
        self.compute_internal_gains(context)
        
        y = []
        m_vn = np.zeros(N)
        theta_phi = np.array([293.15, 293.15, 293.15, 0.0, 0.0])
        
        B0 = self.B0.copy()
        B1 = self.B1.copy()
        
        def compute_temperatures(A, B1, hg_ei, power, theta_e, theta_em_prev, phi_int):
            A = A.copy()
            B1 = B1.copy()
            A[0, 0] = A[0, 0] + hg_ei
            B1[0] = hg_ei
            temps = np.linalg.solve(A[0:3,0:3], B1[0:3]*theta_e[0:3] + B0[0:3]*np.array([0, 0, theta_em_prev]) + phi_int[0:3] - A[0:3,3]*power)
            return temps
        
        def compute_power(A, B1, hg_ei, theta_e, theta_em_prev, phi_int, setpoint):
            A = A.copy()
            B1 = B1.copy()
            A[0, 0] = A[0, 0] + hg_ei
            B1[0] = hg_ei
            theta_phi = np.linalg.solve(A, B1*theta_e + B0*np.array([0, 0, theta_em_prev, 0]) + phi_int + np.array([0, 0, 0, setpoint]))
            return theta_phi[3]

        
        def delta(m, *data):
            A, B1, theta_e, theta_em_prev, phi_int, setpoint = data
            hg_ei = m*(constants.c_a + constants.c_v*context.air_humidity[i])
            temps = compute_temperatures(A, B1, hg_ei, 0, theta_e, theta_em_prev, phi_int)
            delta = np.dot(A[3, 0:3], temps) - setpoint
            return delta
        
        
        for i in range(N):
            
            # Use the heating until april 15th and from october 15th (winter)
            # and the cooling matrix otherwise (summer)
            # to compute the "free" temperatures evolution
            if i > 2520 and i < 6912:
                A = self.F_cooling.copy()
            else:
                A = self.F_heating.copy()

            B1 = self.B1.copy()

            # -------------------------------------------------------
            # Solve the system with heating/cooling power set to zero ("free" evolution)
            theta_em_prev = theta_phi[2]
            theta_e = self.compute_equivalent_temperatures_at_timestep(context, i, theta_phi[0])
            theta = compute_temperatures(A, B1, self.hg_ei[i], 0, theta_e, theta_em_prev, self.phi_int[i,:])
            theta_op = 0

            setpoint_met = False
            
            # -------------------------------------------------------
            # If air temperature is below the heating setpoint
            if np.dot(self.F_heating[3, 0:3], theta) < self.heating_setpoint[i]:
                
                # -------------------------------------------------------
                # Find if increasing the natural ventilation flowrate could be sufficient to reach the setpoint
                # First test if external temperature is high enough
                if self.theta_e[i, 0] > self.heating_setpoint[i]:
                    
                    if self.natural_ventilation is True and self.natural_ventilation_profile[i] > 0.9:
                
                        # Then test if increasing flowrate would increase temperature
                        m = 1
                        hg_ei = m*(constants.c_a + constants.c_v*context.air_humidity[i])
                        theta_inc = compute_temperatures(A, B1, hg_ei, 0, theta_e, theta_em_prev, self.phi_int[i,:])

                        if theta_inc[0] > theta[0]:

                            # Then find the flowrate required to reach the setpoint
                            m = minimize(delta, 0, xtol = 0.01, args = (self.F_heating.copy(),
                                                         self.B1.copy(),
                                                         self.theta_e[i,:],
                                                         theta_em_prev,
                                                         self.phi_int[i,:],
                                                         self.heating_setpoint[i]))

                            # Check if the solver found a solution and the flowrate is high enough compared to the required hygienic flowrate
                            if m > self.hygienic_mass_flowrate[i]:

                                m = np.minimum(m, self.natural_ventilation_max_acr*self.volume/3600/1.22)
                                hg_ei = m*(constants.c_a + constants.c_v*context.air_humidity[i])

                                # Compute the resulting temperatures 
                                temps = compute_temperatures(A, B1, hg_ei, 0, theta_e, theta_em_prev, self.phi_int[i,:])

                                m_vn[i] = m
                                theta_phi[0:3] = temps
                                theta_phi[3] = 0

                                # Check if the setpoint is met
                                if abs(np.dot(self.F_heating[3, 0:3], temps) - self.heating_setpoint[i]) < 0.1:
                                    setpoint_met = True
                

                # -------------------------------------------------------
                # If natural ventilation is not enough to reach the setpoint
                if setpoint_met is False and self.heating is True and self.heating_system_profile[i] > 0.9:
                    
                    # Compute the power needed to reach the setpoint and the temperatures of the surface and mass
                    phi = compute_power(A, B1, self.hg_ei[i], theta_e, theta_em_prev, self.phi_int[i,:], self.heating_setpoint[i])
                    
                    # If the power exceeds the maximum available power
                    if phi > self.max_heating_power:

                        # Cap the power and recompute the air temperature level
                        theta_phi[0:3] = compute_temperatures(A, B1, self.hg_ei[i], self.max_heating_power, theta_e, theta_em_prev, self.phi_int[i,:])
                        theta_phi[3] = self.max_heating_power

                    # Else the air temperature can reach the setpoint
                    else:
                        theta_phi[0:3] = compute_temperatures(A, B1, self.hg_ei[i], phi, theta_e, theta_em_prev, self.phi_int[i,:])
                        theta_phi[3] = phi

                theta_phi[4] = np.dot(self.F_heating[3, 0:4], theta_phi[0:4])
                
            
            # -------------------------------------------------------
            # If air temperature is above the cooling setpoint
            elif np.dot(self.F_cooling[3, 0:3], theta) > self.cooling_setpoint[i]:
                
                A = self.F_cooling.copy()
                B1 = self.B1.copy()
                
                # -------------------------------------------------------
                # Find if increasing the natural ventilation flowrate could be sufficient to reach the setpoint
                # First test if external temperature is low enough
                #if self.theta_e[i, 0] < self.cooling_setpoint[i]:
                    
                if self.natural_ventilation is True and self.natural_ventilation_profile[i] > 0.9 and self.theta_e[i,0] > self.natural_ventilation_ext_temp_lower_limit:
            
                    # Then test if increasing flowrate would decrease temperature
                    m = 1
                    hg_ei = m*(constants.c_a + constants.c_v*context.air_humidity[i])
                    theta_inc = compute_temperatures(A, B1, hg_ei, 0, theta_e, theta_em_prev, self.phi_int[i,:])

                    if theta_inc[0] < theta[0]:

                        # Then find the flowrate required to reach the setpoint
                        m = fsolve(delta, 0, xtol = 0.001, args = (self.F_cooling.copy(),
                                                     self.B1.copy(),
                                                     theta_e,
                                                     theta_em_prev,
                                                     self.phi_int[i,:],
                                                     self.cooling_setpoint[i]))


                        # Check if the required flowrate is sufficient to meet the air renewal needs, and is not above the VN system max flowrate
                        if m > self.hygienic_mass_flowrate[i]:

                            self.hygienic_mass_flowrate[i] = 0

                            q_max = 0.5*self.natural_ventilation_opening_area*np.sqrt(0.001*np.power(context.wind_speed[i], 2) + 0.0035*1.5*np.abs(theta_e[0] - theta[0]))

                            #m = np.minimum(m, 1.22*self.natural_ventilation_max_acr*self.volume/3600)
                            m = np.minimum(m, q_max)

                            hg_ei = m*(constants.c_a + constants.c_v*context.air_humidity[i])

                            
                            # hg_ei_max = 1.22*q_max*(constants.c_a + constants.c_v*context.air_humidity[i])
                            # hg_ei = np.minimum(self.hg_ei[i]*q, hg_ei_max)
                            # q = hg_ei/self.hg_ei[i]

                            # Compute the resulting temperatures
                            temps = compute_temperatures(A, B1, hg_ei, 0, theta_e, theta_em_prev, self.phi_int[i,:])
                            #temps = compute_temperatures(A, B1, hg_ei, 1, 0, self.theta_e[i,:], theta_em_prev, self.phi_int[i,:])
                            
                            m_vn[i] = m
                            theta_phi[0:3] = temps
                            theta_phi[3] = 0

                            # Check if the setpoint is met
                            if abs(np.dot(self.F_cooling[3, 0:3], temps) - self.cooling_setpoint[i]) < 0.1:
                                setpoint_met = True
                
                # -------------------------------------------------------
                # If natural ventilation is not enough to reach the setpoint
                if setpoint_met is False and self.cooling is True and self.cooling_system_profile[i] > 0.9:

                    m_vn[i] = 0
                    
                    # Compute the power needed to reach the setpoint and the temperatures of the surface and mass
                    phi = compute_power(A, B1, self.hg_ei[i], theta_e, theta_em_prev, self.phi_int[i,:], self.cooling_setpoint[i])

                    # If the power exceeds the maximum available power
                    if abs(phi) > self.max_cooling_power:

                        # Cap the power and recompute the air temperature level
                        theta_phi[0:3] = compute_temperatures(A, B1, self.hg_ei[i], -self.max_cooling_power, theta_e, theta_em_prev, self.phi_int[i,:])
                        theta_phi[3] = -self.max_cooling_power

                    # Else the air temperature can reach the setpoint
                    else:
                        theta_phi[0:3] = compute_temperatures(A, B1, self.hg_ei[i], phi, theta_e, theta_em_prev, self.phi_int[i,:])
                        theta_phi[3] = phi
                        
                theta_phi[4] = np.dot(self.F_cooling[3, 0:4], theta_phi[0:4])
            
            # Else no heating / cooling power is needed
            else:
                theta_phi[0:3] = theta
                theta_phi[3] = 0
                theta_phi[4] = np.dot(self.F_heating[3, 0:4], theta_phi[0:4])
                setpoint_met = True

            
            y.append(theta_phi.copy())

        y = np.array(y)
        
        self.theta_i = y[:, 0]
        self.theta_s = y[:, 1]
        self.theta_m = y[:, 2]
        self.phi = y[:, 3]
        self.theta_op = y[:, 4]
        self.m_vn = m_vn
        self.p_vn = m_vn*(constants.c_a + constants.c_v*context.air_humidity)*(context.air_temperature - self.theta_i)
        # self.irr = self.opaque_faces[0].i_sr


