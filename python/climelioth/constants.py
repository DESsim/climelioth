
class constants:
    h_e = 25
    h_re = 5
    h_ri = 5.5          # Interior radiative exchange coefficient [W.m-2.K-1]
    h_ci = 2.5          # Interior convective exchange coefficient [W.m-2.K-1]
    h_rs = 1.2*h_ri
    h_is = h_ci + h_rs  # Corrected coefficient for radiative exchanges
    c_a = 1006          # Heat capacity of air [J.kg-1.K-1]
    c_v = 1830          # Heat capacity of water [J.kg-1.K-1]
    h_fg = 25e5         # Latent evaporation heat of water
    f_sa = 0.1


