from .group import Group
from .face import Face, OpaqueFace, TransparentFace
from .solar_mask import DistantSolarMask, NearHorizontalSolarMask, NearVerticalLeftSolarMask, NearVerticalRightSolarMask, AzimuthElevationMask
from .airflow import Airflow, HygienicAirflow
from .schedule import Schedule
from .blinds import Blinds
from .internal_gain import InternalGain, LightingGain
from .node import Node

import json
import numpy as np
import pandas as pd

def simulate(timebase, delta_t, context, floor_part, use, use_share, parameters_file_path, schedules_file_path, output_dir):

    # Load custom or standard parameters for the STD
    with open(parameters_file_path, "r") as file:
        parameters = json.load(file)

    detailed_uses = parameters['detailed_uses']

    # Geometry
    floor_area = floor_part['area']*0.85

    ceiling_height = floor_part['height']-0.5
    volume = floor_area*ceiling_height
    internal_area = 2*floor_area + floor_part['perimeter']*ceiling_height

    # Thermal characteristics
    mass_area_ratio=parameters['hvac']['thermal_inertia']['mass_area_ratio']
    mass_heat_capacity=parameters['hvac']['thermal_inertia']['mass_heat_capacity']

    u_roof = parameters['roof']['u_roof']

    glazing_ratio = parameters['facade']['windows']['glazing_ratio']

    window_sw_1 = parameters['facade']['windows']['window_sw_1']
    window_sw_2 = parameters['facade']['windows']['window_sw_2']
    window_transparent_ratio = parameters['facade']['windows']['window_transparent_ratio']

    wall_absorption_coef = parameters['facade']['walls']['solar_absorption_coeff']
    roof_absorption_coef = parameters['roof']['solar_absorption_coeff']

    # Blinds
    radiation_threshold = parameters['facade']['shading_devices']['blinds']['radiation_threshold']
    shading_coefficient = parameters['facade']['shading_devices']['blinds']['shading_coeff']

    # Ventilation
    if parameters['hvac']['ventilation']['natural_ventilation']['enabled'] == 1:
        vn = True
        ext_lower = 273.15+parameters['hvac']['ventilation']['natural_ventilation']['ext_temp_limit']
        max_acr = parameters['hvac']['ventilation']['natural_ventilation']['max_acr']
        opening_ratio = parameters['hvac']['ventilation']['natural_ventilation']['opening_ratio']
    else:
        vn = False

    # Thermal group
    g = Group(floor_area = floor_area,
              volume = volume,
              mass_area_ratio = mass_area_ratio,
              mass_heat_capacity = mass_heat_capacity,
              internal_area = internal_area)

    walls = floor_part['walls']
    total_wall_area = 0
    total_window_area = 0


    # -------------------------------
    # Walls
    def get_value(par, orientation):
        if type(par) is dict:
            return par[orientation]
        else:
            return par


    for w in walls:
        # with open("irr.csv","a") as file:
                # file.write("\nwall,")
        if w['adjacent'] is False:


            if w['azimuth'] < 45 and w['azimuth'] > -45:
                u_wall = get_value(parameters['facade']['walls']['u_wall'], 'south')
                u_window = get_value(parameters['facade']['windows']['u_window'], 'south')
                glazing_ratio = get_value(parameters['facade']['windows']['glazing_ratio'], 'south')
                window_sw_1 = get_value(parameters['facade']['windows']['window_sw_1'], 'south')
                window_sw_2 = get_value(parameters['facade']['windows']['window_sw_1'], 'south')
                window_transparent_ratio = get_value(parameters['facade']['windows']['window_transparent_ratio'], 'south')
                fixed_shd_top_width = parameters['facade']['shading_devices']['fixed_shading']['south']['top_width']
                fixed_shd_sides_width = parameters['facade']['shading_devices']['fixed_shading']['south']['sides_width']
            
            elif w['azimuth'] <= 45+90:
                u_wall = get_value(parameters['facade']['walls']['u_wall'], 'west')
                u_window = get_value(parameters['facade']['windows']['u_window'], 'west')
                glazing_ratio = get_value(parameters['facade']['windows']['glazing_ratio'], 'west')
                window_sw_1 = get_value(parameters['facade']['windows']['window_sw_1'], 'west')
                window_sw_2 = get_value(parameters['facade']['windows']['window_sw_1'], 'west')
                window_transparent_ratio = get_value(parameters['facade']['windows']['window_transparent_ratio'], 'west')
                fixed_shd_top_width = parameters['facade']['shading_devices']['fixed_shading']['west']['top_width']
                fixed_shd_sides_width = parameters['facade']['shading_devices']['fixed_shading']['west']['sides_width']
            
            elif w['azimuth'] >= -45-90:
                u_wall = get_value(parameters['facade']['walls']['u_wall'], 'east')
                u_window = get_value(parameters['facade']['windows']['u_window'], 'east')
                glazing_ratio = get_value(parameters['facade']['windows']['glazing_ratio'], 'east')
                window_sw_1 = get_value(parameters['facade']['windows']['window_sw_1'], 'east')
                window_sw_2 = get_value(parameters['facade']['windows']['window_sw_1'], 'east')
                window_transparent_ratio = get_value(parameters['facade']['windows']['window_transparent_ratio'], 'east')
                fixed_shd_top_width = parameters['facade']['shading_devices']['fixed_shading']['east']['top_width']
                fixed_shd_sides_width = parameters['facade']['shading_devices']['fixed_shading']['east']['sides_width']
            
            else:
                u_wall = get_value(parameters['facade']['walls']['u_wall'], 'north')
                u_window = get_value(parameters['facade']['windows']['u_window'], 'north')
                glazing_ratio = get_value(parameters['facade']['windows']['glazing_ratio'], 'north')
                window_sw_1 = get_value(parameters['facade']['windows']['window_sw_1'], 'north')
                window_sw_2 = get_value(parameters['facade']['windows']['window_sw_1'], 'north')
                window_transparent_ratio = get_value(parameters['facade']['windows']['window_transparent_ratio'], 'north')
                fixed_shd_top_width = parameters['facade']['shading_devices']['fixed_shading']['north']['top_width']
                fixed_shd_sides_width = parameters['facade']['shading_devices']['fixed_shading']['north']['sides_width']
            

            wall_area = w['area']*(1-glazing_ratio)
            window_area = w['area']*glazing_ratio

            total_wall_area += wall_area
            total_window_area += window_area

            mask = AzimuthElevationMask(w['mask'])

            # Opaque and transparent faces
            of = OpaqueFace(area=wall_area,
                            azimuth=w['azimuth'],
                            tilt=90,
                            absorption_coef=wall_absorption_coef,
                            u_value=u_wall,
                            altitude=w['altitude'],
                            output_dir=output_dir)

            of.add_masks([mask])

            g.add_faces([of])

            if window_area > 0: 
                tf = TransparentFace(area=window_area,
                                 azimuth=w['azimuth'],
                                 tilt=90,
                                 sw_1=window_sw_1,
                                 sw_2=window_sw_2,
                                 u_value=u_window,
                                 altitude=w['altitude'],
                                 height=1.5,
                                 width=window_area/1.5,
                                 transparent_ratio = window_transparent_ratio)

                blinds = Blinds(radiation_threshold = radiation_threshold,
                            shading_coefficient = shading_coefficient)

                tf.add_blinds(blinds)

                tf.add_masks([mask])

                wmask = NearHorizontalSolarMask(width=fixed_shd_top_width, distance_from_window=0)
                tf.add_masks([wmask])
            
                lmask = NearVerticalLeftSolarMask(width=fixed_shd_sides_width, distance_from_window=0)
                tf.add_masks([lmask])

                rmask = NearVerticalRightSolarMask(width=fixed_shd_sides_width, distance_from_window=0)
                tf.add_masks([rmask])

                g.add_faces([tf])


    # -------------------------
    # Roofs
    roofs = floor_part['roofs']

    for r in roofs:
        # with open("irr.csv","a") as file:
                # file.write("\nroof,")
        of = OpaqueFace(area=r['area'],
                        azimuth=0,
                        tilt=0,
                        absorption_coef=roof_absorption_coef,
                        u_value=u_roof,
                        altitude=r['altitude'],
                        output_dir=output_dir)

        mask = AzimuthElevationMask(r['mask'])
        of.add_masks([mask])
        g.add_faces([of])

    # Schedules
    s = Schedule(timebase, schedules_file_path, use, detailed_uses)

    s.set_temperature_setpoints(heating_on=parameters['hvac']['heating']['setpoint_on'],
                                heating_off=parameters['hvac']['heating']['setpoint_off'],
                                cooling_on=parameters['hvac']['cooling']['setpoint_on'],
                                cooling_off=parameters['hvac']['cooling']['setpoint_off'])


    # Internal gains
    s.set_internal_gains(
        floor_area = floor_area,
        area_per_occupant = parameters['occupants']['area_per_occupant'],
        equipments_gains = parameters['equipments']['elec_power'],
        lighting_gains = parameters['equipments']['lighting_power'],
        detailed_uses_share = parameters['detailed_uses_share']
    )

    s.set_hotwater_needs(hw_volume = parameters['hvac']['hot_water']['consumption']*floor_area)
    s.set_cooling_process_needs(
        floor_area = floor_area,
        cooling_power = parameters['equipments']['cooling_power'],
        detailed_uses_share = parameters['detailed_uses_share']
    )

    # Occupants
    N_occupants = 0
    for det_use in detailed_uses:
        if parameters['occupants']['area_per_occupant'][det_use] > 0:
            N_occupants += floor_area*parameters['detailed_uses_share'][det_use]/parameters['occupants']['area_per_occupant'][det_use]

    if parameters['hvac']['ventilation']['flowrate_per_occupant_on'] is not None:
        acr_on = N_occupants*parameters['hvac']['ventilation']['flowrate_per_occupant_on']/volume
        acr_off = N_occupants*parameters['hvac']['ventilation']['flowrate_per_occupant_off']/volume
    else:
        acr_on = parameters['hvac']['ventilation']['acr_on']
        acr_off = parameters['hvac']['ventilation']['acr_off']

    s.set_airchange_rates(acr_on=acr_on, acr_off=acr_off)

    # Air flows
    if parameters['hvac']['ventilation']['dual_flow_ventilation'] == 1:
        reco_rate = parameters['hvac']['ventilation']['heat_recovery_efficiency']
        t_ext = context.air_temperature - 273.15
        supply_temperature = context.air_temperature.copy()

        if parameters['hvac']['heating']['system_type'] != "none":
            supply_temperature[t_ext < 16] = 273.15 + 16

        if parameters['hvac']['cooling']['system_type'] != "none":    
            supply_temperature[t_ext > 26] = 273.15 + 26

    else:
        reco_rate = 0.0
        supply_temperature = context.air_temperature.copy()

    af = HygienicAirflow(humidity=context.air_humidity,
                     mass_flowrate=1.22*volume*s.profiles['air_change_rate'].values/3600,
                     temperature=supply_temperature,
                     heat_recovery_rate = reco_rate)

    # Compute the infiltrations depending on the wind speed at zone height
    # (adapted from Arnaud Sanson approximation)
    z = walls[0]['altitude'] + 0.5*walls[0]['height']
    wind_speed = context.wind_speed*np.log(z/1.0)/np.log(10.0/1.0)
    q4pa = parameters['hvac']['ventilation']['infiltration_rate']
    infiltration_profile = q4pa*15*np.power(wind_speed/30, 4/3)

    inf = Airflow(humidity=context.air_humidity,
                     mass_flowrate=infiltration_profile*1.22*(total_wall_area+total_window_area)/3600,
                     temperature=context.air_temperature)

    g.add_airflows([af, inf])

    # Internal gains
    og = InternalGain(s.profiles['occ_gains'])
    eg = InternalGain(s.profiles['other_gains'])
    lg = LightingGain(s.profiles['lighting_gains'])

    g.add_internal_gains([og, eg, lg])

    # heating/cooling systems
    g.set_temperature_setpoints(cooling_setpoint = s.profiles['cooling_setpoint'].values+273.15,
                                heating_setpoint = s.profiles['heating_setpoint'].values+273.15)

    if parameters['hvac']['heating']['system_type'] != "none":
        heating_profile = 1
    else:
        heating_profile = 0

    g.add_heating_system(system_type=parameters['hvac']['heating']['system_type'], profile=heating_profile*np.ones(len(context.air_temperature)))

    if parameters['hvac']['cooling']['system_type'] != "none":
        cooling_profile = 1
    else:
        cooling_profile = 0

    g.add_cooling_system(system_type=parameters['hvac']['cooling']['system_type'], profile=cooling_profile*np.ones(len(context.air_temperature)))

    if vn is True:
        g.set_natural_ventilation(profile=np.ones(len(context.air_temperature)),
                                  max_acr = max_acr*3600/delta_t,
                                  ext_temp_lower_limit=ext_lower,
                                  opening_ratio = opening_ratio)

    g.simulate(context)

    # Heating (terminal)
    p_heat_term = g.phi.copy()
    p_heat_term[p_heat_term < 0] = 0

    # Cooling (terminal)
    p_cool_term = g.phi.copy()
    p_cool_term[p_cool_term > 0] = 0
    
    # Cooling / Heating (AHU)
    p_heat_ahu = np.zeros(len(p_heat_term))
    p_cool_ahu = np.zeros(len(p_heat_term))

    # Heating / cooling recovery on extracted air
    p_heat_ahu_reco = np.zeros(len(p_heat_term))
    p_cool_ahu_reco = np.zeros(len(p_heat_term))

    if parameters['hvac']['ventilation']['dual_flow_ventilation'] == 1:

        k_vent = 2

        # Add AHU heating load
        t_int_p = np.roll(g.theta_i, 1)

        t_after_reco = context.air_temperature + reco_rate*(t_int_p - context.air_temperature)

        # If heating is needed, limit the recovery not to exceed the supply temperature
        t_after_reco[supply_temperature - context.air_temperature > 0.0] = np.minimum(
            t_after_reco[supply_temperature - context.air_temperature > 0.0],
            supply_temperature[supply_temperature - context.air_temperature > 0.0]
        )

        # If heating is needed and the recovery leads to a supply temperature lower than the temperature of exterior air
        # bypass the recovery and set the temperature after recovery to exterior temperature
        t_after_reco[(supply_temperature - context.air_temperature > 0.0) & (t_after_reco < context.air_temperature)] = context.air_temperature[(supply_temperature - context.air_temperature > 0.0) & (t_after_reco < context.air_temperature)]

        # If cooling is needed, limit the recovery not to be inferior to the supply temperature
        t_after_reco[supply_temperature - context.air_temperature < 0.0] = np.maximum(
            t_after_reco[supply_temperature - context.air_temperature < 0.0],
            supply_temperature[supply_temperature - context.air_temperature < 0.0]
        )

        # If cooling is needed and the recovery leads to a supply temperature greater than the temperature of exterior air
        # bypass the recovery and set the temperature after recovery to exterior temperature
        t_after_reco[(supply_temperature - context.air_temperature < 0.0) & (t_after_reco > context.air_temperature)] = context.air_temperature[(supply_temperature - context.air_temperature < 0.0) & (t_after_reco > context.air_temperature)]

        # If no cooling nor heating is needed, set the recovery to zero
        t_after_reco[supply_temperature == context.air_temperature] = supply_temperature[supply_temperature == context.air_temperature]

        p_ahu = af.mass_flowrate/1.22*1006*(supply_temperature - context.air_temperature)
        p_heat_ahu = np.where(p_ahu > 0, p_ahu, 0)
        p_cool_ahu = np.where(p_ahu < 0, p_ahu, 0)

        # Compute recovery power
        p_reco = af.mass_flowrate/1.22*1006*(t_after_reco - context.air_temperature)
        p_heat_ahu_reco = np.where(supply_temperature - context.air_temperature > 0.0, p_reco, 0)
        p_cool_ahu_reco = np.where(supply_temperature - context.air_temperature < 0.0, p_reco, 0)

    else:

        k_vent = 1

    # Ventilation electrical consumption
    # https://energieplus-lesite.be/evaluer/ventilation4/evaluer-la-consommation-de-la-ventilation/
    p_vent = g.hygienic_mass_flowrate/1.22*k_vent*1500/0.6/2

    p_hw = 4185/3600*s.profiles[ 'hw_volume' ]*(parameters['hvac']['hot_water']['temperature'] + 273.15 - context.water_temperature)
    
    hw_volume = s.profiles[ 'hw_volume' ]/1000 #l to m3
    # Equipments, lighting and cooling process power
    p_equi = g.internal_gains[1].power
    p_light = g.internal_gains[2].power
    p_cooling_process = -s.profiles['cooling_process']

    


    # Scale the power according to the floor area share of the use
    share = use_share

    floor_area = share*floor_area

    p_heat_term = share*p_heat_term
    p_heat_ahu = share*p_heat_ahu
    p_heat_ahu_reco = share*p_heat_ahu_reco
    p_heat = p_heat_term + p_heat_ahu

    p_cool_term = share*p_cool_term
    p_cool_ahu = share*p_cool_ahu
    p_cool_ahu_reco = share*p_cool_ahu_reco
    p_cool = p_cool_term + p_cool_ahu

    p_cool_process = share*p_cooling_process
    p_hw = share*p_hw
    p_vent = share*p_vent
    p_light = share*p_light
    p_equi = share*p_equi
    
    # # Auxiliary electrical consumption for heating and cooling
    # MOVED to run.py
    p_aux = np.zeros(8760)
    # # Initialize
    # water_flow = np.zeros(8760)
    # deltaT = 0
    # #Heating
    # if parameters['hvac']['heating']['system_type'] == "radiators" or parameters['hvac']['heating']['system_type'] == "convective":
    #     deltaT = 40
    # elif parameters['hvac']['heating']['system_type'] == "radiant_ceiling":
    #     deltaT = 10
    # water_flow += p_heat / (1160  * deltaT) # W / (Wh/(m3*K) * K) = m3/h
    
    # #cooling
    # if parameters['hvac']['cooling']['system_type'] == "convective":
    #     deltaT = 40 #K
    # elif parameters['hvac']['cooling']['system_type'] == "radiant_ceiling":
    #     deltaT = 10 #K
    # water_flow += np.absolute(p_cool) / (1160  * deltaT) # W / (Wh/(m3*K) * K) = m3/h
    
    # dhw
    # water_flow += hw_volume
    # pump_cons = 4000 #W/m3/h #https://www.researchgate.net/profile/Bryan_Karney/publication/263927622/figure/fig2/AS:296615354093574@1447730019808/Sample-testing-results-for-100-hp-75-kW-pump-power-consumption_W640.jpg
    
    # p_aux = pump_cons * water_flow
    
    
    df = pd.DataFrame({'timebase': timebase,
                       'floor_area': floor_area,
                       'use': use,
                       'p_heat': p_heat,
                       'p_heat_reco': p_heat_ahu_reco,
                       'p_cool': p_cool,
                       'p_cool_reco': p_cool_ahu_reco,
                       'p_cool_process': p_cool_process,
                       'p_hw': p_hw,
                       'p_vent': p_vent,
                       'p_light': p_light,
                       'p_equi': p_equi,
                       "p_aux": p_aux,
                       "system_heating": parameters['hvac']['heating']['system_type'],
                       "system_cooling": parameters['hvac']['cooling']['system_type'],
                       "hw_volume": hw_volume})

    # For debugging
    # df = pd.DataFrame({'timebase': timebase,
    #                    'floor_area': floor_area,
    #                    'use': use,
    #                    'p_heat': p_heat,
    #                    'p_heat_term': p_heat_term,
    #                    'p_heat_ahu': p_heat_ahu,
    #                    'p_heat_reco': p_heat_ahu_reco,
    #                    'p_cool': p_cool,
    #                    'p_cool_term': p_cool_term,
    #                    'p_cool_ahu': p_cool_ahu,
    #                    'p_cool_reco': p_cool_ahu_reco,
    #                    'p_cool_process': p_cool_process,
    #                    'p_hw': p_hw,
    #                    'p_vent': p_vent,
    #                    'p_light': p_light,
    #                    'p_equi': p_equi,
    #                    't_int': g.theta_i,
    #                    't_ext': context.air_temperature,
    #                    't_supply': supply_temperature,
    #                    't_reco': t_after_reco})

    # with open("test.csv","a") as file:
        # file.write("{},{},{},{}\n".format(use,floor_area,total_wall_area,total_window_area))

    return df
