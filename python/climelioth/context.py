import numpy as np

class Context:

    def __init__(self, delta_t, ground_albedo=0.2):
        self.delta_t = delta_t
        self.ground_albedo = ground_albedo

    def set_weather(self, air_temperature, water_temperature,
                    sky_temperature, air_humidity,
                    direct_normal_irradiation, diffuse_horizontal_irradiation,
                    solar_elevation, solar_azimuth,
                    wind_speed):
    
        self.air_temperature = air_temperature
        self.water_temperature = water_temperature
        self.air_humidity = air_humidity
        self.sky_temperature = sky_temperature
        self.direct_normal_irradiation = direct_normal_irradiation
        self.diffuse_horizontal_irradiation = diffuse_horizontal_irradiation
        self.solar_elevation = solar_elevation
        self.solar_azimuth = solar_azimuth
        self.wind_speed = wind_speed

        self.direct_lighting = direct_normal_irradiation*(-1.03753210e-8*np.power(solar_elevation, 6) + \
                                                          2.90312257e-6*np.power(solar_elevation, 5) - \
                                                          3.31804423e-4*np.power(solar_elevation, 4) + \
                                                          1.99283162e-2*np.power(solar_elevation, 3) - \
                                                          6.72171072e-1*np.power(solar_elevation, 2) + \
                                                          1.24650445*np.power(solar_elevation, 1) + \
                                                          2.38954889)

        self.diffuse_lighting = np.ones(len(solar_elevation))*diffuse_horizontal_irradiation
        self.diffuse_lighting[ direct_normal_irradiation < 1 ] = 124*diffuse_horizontal_irradiation[ direct_normal_irradiation < 1 ]
        self.diffuse_lighting[ direct_normal_irradiation > 120 ] = 128*diffuse_horizontal_irradiation[ direct_normal_irradiation > 120 ]

