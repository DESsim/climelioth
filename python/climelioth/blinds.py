import numpy as np

class Blinds:

	def __init__(self, radiation_threshold, shading_coefficient):
		self.radiation_threshold = radiation_threshold
		self.shading_coefficient = shading_coefficient

	def compute_shading(self, radiation):
		shading = np.ones(len(radiation))
		shading[ radiation > self.radiation_threshold ] = self.shading_coefficient
		return shading