import numpy as np

class Node:

	def __init__(self, name, dispatchable=False, storage=False, power=None, max_power=None, min_power=None, max_energy=None):
		self.name = name
		self.dispatchable = dispatchable
		self.children = []
		self.max_power = max_power
		self.min_power = min_power
		self.max_energy = max_energy
		self.storage = storage
		self.energy = np.zeros(8760)
		if power is not None:
			self.power = power
		else:
			self.power = np.zeros(8760)

	def add_children(self, children):
		self.children.extend(children)

	def power_balance(self, t, parent_power_balance=None):

		net_load = 0

		# Add the power production/load of each non dispatchable child
		# (heat load, solar PV production...)
		for child in self.children:
			if child.dispatchable is False and child.storage is False:
				net_load += child.power_balance(t)

		# Add the net load of the parent node
		if parent_power_balance is not None:
			net_load += parent_power_balance

		# Cover the net load with dispatchable children
		for child in self.children:
			if child.dispatchable is True or child.storage is True:
				net_load += child.power_balance(t, net_load)

		if self.dispatchable:
			self.power[t] = np.minimum(- net_load, self.max_power)
			#self.power = np.maximum(self.power, self.min_power)

		if self.storage:
			if net_load >= 0:
				self.power[t] = np.maximum(-net_load, -(self.max_energy - self.energy[t-1]))
			else:
				self.power[t] = np.minimum(-net_load, self.energy[t-1])
			self.energy[t] = self.energy[t-1] - self.power[t]

		if parent_power_balance is not None:
			power = net_load + self.power[t] - parent_power_balance
		else:
			power = net_load + self.power[t]

		return power

	




		

