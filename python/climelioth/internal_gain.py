import numpy as np

class InternalGain:

    def __init__(self, power, convective_fraction=0.5):
        self.power = power
        self.convective_fraction = convective_fraction


class OccupantsHeatGain(InternalGain):

    def __init__(self, N_occupants):
        power = N_occupants*90
        InternalGain.__init__(self, power, convective_fraction=0.5)


class LightingGain(InternalGain):

    def __init__(self, power):
        InternalGain.__init__(self, power, convective_fraction=0.5)

    def modulate(self, phi):
        profile = phi.copy()

        # phi_100 = 10
        # phi_0 = 20
        # profile[phi < phi_100] = 1
        # profile[(phi >= phi_100) & (phi <= phi_0)] = (phi_0 - phi[(phi >= phi_100) & (phi <= phi_0)])/(phi_0 - phi_100)
        # profile[phi > phi_0] = 0

        ecl = phi*100*0.5
        profile[ecl < 100] = 1
        profile[(ecl >= 100) & (ecl < 700)] = 1 - 0.7*(ecl[(ecl >= 100) & (ecl < 700)] - 100)/(700 - 100)
        profile[ecl >= 700] = 0.3 - 0.3*(ecl[ecl >= 700] - 700)/(2800 - 700)
        profile = np.minimum(1, profile)
        self.power = profile*self.power
        

