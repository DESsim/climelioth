import pandas as pd
import numpy as np
import os.path
from functools import reduce

class Schedule:


	def __init__(self, timebase, schedules_path, use, detailed_uses):

		detailed_profiles = {}
		timebase = timebase.to_series()

		for det_use in detailed_uses:
			
			ref_df = pd.DataFrame({'time': timebase,
									 'week': timebase.dt.week,
									  'wday': timebase.dt.weekday,
									  'hour': timebase.dt.hour,
									  'yweek': np.floor(timebase.dt.dayofyear/7)})

			pkgpath = os.path.abspath(os.path.dirname(__file__))
			profiles_files = [
				{'variable': 'wday_hour_internal_heat_gains_occupants', 'folder': det_use},
				{'variable': 'wday_hour_internal_heat_gains_equipments', 'folder': det_use},
				{'variable': 'wday_hour_hot_water_draw', 'folder': ''},
				{'variable': 'wday_hour_lighting', 'folder': ''},
				{'variable': 'wday_hour_ventilation', 'folder': ''}
			]
			
			dfs = []
			for f in profiles_files:
				profile = pd.read_csv(os.path.join(schedules_path, f['folder'],  f['variable'] + '.csv'), dtype=np.float64)
				profile = profile.melt('wday')
				profile.columns = ['wday', 'hour', f['variable']]
				profile['hour'] = pd.to_numeric(profile['hour'])
				dfs.append(profile)

			profiles = reduce(lambda x, y: pd.merge(x, y, on = ['wday', 'hour']), dfs)

			profiles = pd.merge(ref_df, profiles, on = ['wday', 'hour'])

			holidays = pd.read_csv(schedules_path + '/week_occupation.csv', dtype=np.float64)
			profiles = pd.merge(profiles, holidays, on='week')

			profiles.sort_values('time', inplace=True)

			profiles['wday_hour_occupation'] = np.where(profiles['wday_hour_internal_heat_gains_occupants'] > 0, 1, 0)
			profiles['detailed_use'] = det_use

			detailed_profiles[det_use] = profiles

		self.profiles = profiles.copy()
		self.detailed_profiles = detailed_profiles


	def set_temperature_setpoints(self, heating_on, heating_off, cooling_on, cooling_off):

		self.profiles[ 'heating_setpoint' ] = heating_off 
		self.profiles.loc[ self.profiles['wday_hour_internal_heat_gains_occupants'] > 0.0, 'heating_setpoint' ] = heating_on

		# Ramp up heating in the morning
		# t_change = np.diff(self.profiles['heating_setpoint'])
		# index = numpy.where(t_change > 0) + 1
		# self.profiles.loc[index - 2, 'heating_setpoint'] = heating_off + (heating_on-heating_off)/3
		# self.profiles.loc[index - 1, 'heating_setpoint'] = heating_off + 2*(heating_on-heating_off)/3

		self.profiles[ 'cooling_setpoint' ] = cooling_off 
		self.profiles.loc[ self.profiles['wday_hour_internal_heat_gains_occupants'] > 0.0, 'cooling_setpoint' ] = cooling_on

	def set_airchange_rates(self, acr_on, acr_off):
		self.profiles[ 'air_change_rate' ] = acr_off
		self.profiles.loc[ self.profiles['wday_hour_ventilation'] > 0, 'air_change_rate' ] = acr_on

	def set_internal_gains(self, floor_area, area_per_occupant, equipments_gains, lighting_gains, detailed_uses_share):

		self.profiles[ 'occ_gains' ] = 0.0
		self.profiles[ 'other_gains' ] = 0.0
		self.profiles[ 'lighting_gains' ] = 0.0

		for det_use in detailed_uses_share:

			area = floor_area*detailed_uses_share[det_use]
			
			# Occupants gain
			if area_per_occupant[det_use] > 0:
				N_occupants = area/area_per_occupant[det_use]
			else:
				N_occupants = 0.0

			self.profiles[ 'occ_gains' ] += 90*N_occupants*self.detailed_profiles[det_use]['wday_hour_internal_heat_gains_occupants']*self.profiles['holiday_occupation']

			# Equipements gain
			self.profiles[ 'other_gains' ] += area*equipments_gains[det_use]*self.detailed_profiles[det_use]['wday_hour_internal_heat_gains_equipments']*self.profiles['holiday_occupation']

			# Lighting gain
			self.profiles[ 'lighting_gains' ] += area*lighting_gains[det_use]*self.detailed_profiles[det_use]['wday_hour_lighting']*self.profiles['holiday_occupation']


	def set_hotwater_needs(self, hw_volume):
		self.profiles[ 'hw_volume' ] = hw_volume*self.profiles['wday_hour_hot_water_draw']*self.profiles['holiday_occupation']

	def set_cooling_process_needs(self, floor_area, cooling_power, detailed_uses_share):

		self.profiles[ 'cooling_process' ] = 0.0

		for det_use in detailed_uses_share:

			area = floor_area*detailed_uses_share[det_use]
			self.profiles[ 'cooling_process' ] += area*cooling_power[det_use]*self.profiles['wday_hour_occupation']*self.profiles['holiday_occupation']
