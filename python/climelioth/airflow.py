
class Airflow:

    def __init__(self, mass_flowrate, temperature, humidity):
        self.mass_flowrate = mass_flowrate
        self.temperature = temperature
        self.humidity = humidity

class HygienicAirflow(Airflow):

	def __init__(self, mass_flowrate, temperature, humidity, heat_recovery_rate):
		Airflow.__init__(self, mass_flowrate, temperature, humidity)
		self.heat_recovery_rate = heat_recovery_rate
