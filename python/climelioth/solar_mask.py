import numpy as np
import pandas as pd
import json

class AzimuthElevationMask:

    def __init__(self, azimuth_elevation):
        df = pd.DataFrame(azimuth_elevation)
        df.columns = ['psi_bin', 'gamma_mask']
        self.azimuth_elevation = df

    def compute_direct_shading(self, face, context):
        mask = np.ones(len(context.solar_azimuth))
        
        df = pd.DataFrame({'time': np.arange(0, len(context.solar_azimuth)),
                           'psi_bin': np.round(context.solar_azimuth*180/np.pi/5)*5,
                           'gamma': context.solar_elevation*180/np.pi})

        df = pd.merge(df, self.azimuth_elevation, on = 'psi_bin')
        df.sort_values('time', inplace=True)
        df.reset_index(inplace=True)

        mask[ df['gamma'] < df['gamma_mask'] ] = 0

        return mask

    def compute_diffuse_shading(self, face, context):
        df = self.azimuth_elevation[ np.abs(self.azimuth_elevation['psi_bin'] - face.azimuth) < 95 ]
        n_sectors = df.shape[0]
        svf = (1 - np.sin(df['gamma_mask']/180*np.pi)).sum()/n_sectors
        mask = svf * np.ones(len(context.solar_azimuth))
        return mask


class DistantSolarMask:

    def __init__(self, distance, altitude):
        self.distance = distance
        self.altitude = altitude

    def compute_direct_shading(self, face, context):
        mask = np.ones(len(context.solar_azimuth))
        phi = context.solar_azimuth - face.azimuth
        cos_phi = np.cos(phi)
        dhe = self.distance*np.tan(context.solar_elevation)/cos_phi
        mask_relative_altitude = self.altitude - face.altitude
        mask[(cos_phi > 1e-5) & (dhe < mask_relative_altitude)] = 0
        return mask

    def compute_diffuse_shading(self, face, context):
        return np.ones(len(context.solar_azimuth))


class NearVerticalLeftSolarMask:

    def __init__(self, width, distance_from_window):
        self.width = width
        self.distance_from_window = distance_from_window

    def compute_direct_shading(self, face, context):
        mask = np.ones(len(context.solar_azimuth))
        phi = context.solar_azimuth - face.azimuth
        cos_phi = np.cos(phi)
        d_lg = np.maximum(0, self.width*np.tan(phi))
        mask[(cos_phi > 1e-5)] = np.minimum(1, np.maximum(0, 1 - (d_lg[(cos_phi > 1e-5)] -self.distance_from_window)/face.width))
        return mask

    def compute_diffuse_shading(self, face, context):
        return np.ones(len(context.solar_azimuth))


class NearVerticalRightSolarMask:

    def __init__(self, width, distance_from_window):
        self.width = width
        self.distance_from_window = distance_from_window

    def compute_direct_shading(self, face, context):
        mask = np.ones(len(context.solar_azimuth))
        phi = context.solar_azimuth - face.azimuth
        cos_phi = np.cos(phi)
        d_ld = np.maximum(0, -self.width*np.tan(phi))
        mask[(cos_phi > 1e-5)] = np.minimum(1, np.maximum(0, 1 - (d_ld[(cos_phi > 1e-5)] -self.distance_from_window)/face.width))
        return mask

    def compute_diffuse_shading(self, face, context):
        return np.ones(len(context.solar_azimuth))


class NearHorizontalSolarMask:

    def __init__(self, width, distance_from_window):
        self.width = width
        self.distance_from_window = distance_from_window

    def compute_direct_shading(self, face, context):
        mask = np.ones(len(context.solar_azimuth))
        phi = context.solar_azimuth - face.azimuth
        cos_phi = np.cos(phi)
        dh = np.maximum(0, self.width*np.tan(context.solar_elevation)/cos_phi)
        mask[(cos_phi > 1e-5)] = np.minimum(np.maximum(0, 1- (dh[(cos_phi > 1e-5)]-self.distance_from_window)/face.height), 1)
        return mask

    def compute_diffuse_shading(self, face, context):
        theta_g = np.arctan((self.distance_from_window + face.height/2)/self.width)
        mask = theta_g/(np.pi/2)
        return mask
