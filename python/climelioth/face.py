import numpy as np
from .constants import constants 
import os
class Face:

    def __init__(self, area, azimuth, tilt, width=None, height=None, altitude=0, output_dir=""):

        if azimuth > 180:
            azimuth = - (360 - azimuth)
        elif azimuth < -180:
            azimuth = 360 + azimuth

        self.azimuth = azimuth*np.pi/180
        self.tilt = tilt*np.pi/180
        self.altitude = altitude
        self.width = width
        self.height = height
        self.area = area
        self.masks = []
        self.blinds = None
        self.output_dir = output_dir


    def compute_incident_solar_radiation(self, context):
        I_dn = context.direct_normal_irradiation
        I_di = context.diffuse_horizontal_irradiation
        gamma = context.solar_elevation
        psi = context.solar_azimuth
        theta = np.minimum(np.pi/2, np.arccos(np.cos(gamma)*np.sin(self.tilt)*np.cos(psi-self.azimuth)+np.sin(gamma)*np.cos(self.tilt)))
        D_rp = np.cos(theta)*I_dn
        D_fp = I_di*0.5*(1+np.cos(self.tilt))
        I_dh = I_dn*np.sin(gamma)
        R_rp = (I_dh + I_di)*context.ground_albedo*0.5*(1-abs(np.cos(self.tilt)))

        # Compute direct/diffuse shading from masks
        if len(self.masks) > 0:

            direct_shading = np.ones(len(gamma))
            diffuse_shading = np.ones(len(gamma))

            for m in self.masks:
                direct_shading *= m.compute_direct_shading(face=self, context=context)
                diffuse_shading *= m.compute_diffuse_shading(face=self, context=context)

            D_rp = D_rp*direct_shading
            D_fp = D_fp*diffuse_shading

            self.direct_shading = direct_shading
            self.diffuse_shading = diffuse_shading

        I_sr = D_rp + D_fp + R_rp
        if len(self.output_dir) > 0:
            with open(os.path.join(self.output_dir,"irr.txt"),"a") as file:
                file.write("{},".format(I_sr.sum()))

        # Apply the shading coefficient of the blinds
        # (which changes depending on incoming radiation)
        if self.blinds is not None:
            shading = self.blinds.compute_shading(I_sr)
            I_sr = shading*I_sr

        return I_sr

    def add_masks(self, masks):
        for m in masks:
            self.masks.append(m)

    def add_blinds(self, blinds):
        self.blinds = blinds



class OpaqueFace(Face):

    def __init__(self, area, azimuth, tilt, u_value, absorption_coef, altitude=0, output_dir=""):
        Face.__init__(self, area = area, azimuth = azimuth, tilt = tilt, altitude=altitude)
        self.thermal_bridges = []
        self.area = area
        self.u_value = u_value
        self.solar_factor = 0.04*absorption_coef*u_value  # Règles Th-bat – Parois opaques p.27
        self.output_dir = output_dir

    def add_thermal_bridge(self, length, psi):
        tb = ThermalBridge(length, psi)
        self.thermal_bridges.append(tb)

    def compute_heat_loss_coeff(self):
        h_thk = 0
        for tb in self.thermal_bridges:
            h_thk += tb.length * tb.psi
        h_thl = self.area*self.u_value
        self.h_th = h_thk + h_thl

    def compute_ext_radiation_flows(self, context):
        i_sr = self.compute_incident_solar_radiation(context)
        q_er = constants.h_re * (context.sky_temperature - context.air_temperature)*0.5*(1+np.cos(self.tilt))
        self.phi_sh_s = self.area*self.solar_factor*i_sr
        self.phi_sh_vc = self.area*q_er*self.u_value/constants.h_re
        self.phi_sl_s = 0
        self.phi_sl_vc = 0
        for tb in self.thermal_bridges:
            self.phi_sl_s += tb.psi*tb.length*i_sr
            self.phi_sl_vc += tb.psi*tb.length*q_er/constants.h_e
            

class TransparentFace(Face):

    def __init__(self, area, azimuth, tilt, u_value, sw_1, sw_2, altitude=0, width=None, height=None, transparent_ratio=0.8):
        Face.__init__(self, area = area, azimuth = azimuth, tilt = tilt, width = width, height = height, altitude = altitude)
        self.thermal_bridges = []
        self.area = area
        self.u_value = u_value
        self.sw_1 = sw_1
        self.sw_2 = sw_2
        self.transparent_ratio = transparent_ratio

    def compute_heat_loss_coeff(self):
        self.h_th = self.area*self.u_value

    def compute_ext_radiation_flows(self, context):
        i_sr = self.compute_incident_solar_radiation(context)
        q_er = constants.h_re * (context.sky_temperature - context.air_temperature)*0.5*(1+np.cos(self.tilt))
        self.f_s1 = self.area*self.transparent_ratio*self.sw_1*i_sr
        self.f_s2 = self.area*self.transparent_ratio*self.sw_2*i_sr
        self.f_s3 = 0 
        self.f_tvc = self.area*q_er*self.u_value/constants.h_e

