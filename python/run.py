import sys
import subprocess
import getopt
import json
import numpy as np
import pandas as pd
import os
import math
import ratios
import warnings
import replaceSTD


# Find the path of the script
script_path = os.path.abspath(__file__)
module_path = os.path.dirname(os.path.dirname(script_path))
parent_path = os.path.dirname(os.path.dirname(module_path))


def main(argv):
    
    # Find the path of the script
    script_path = os.path.abspath(__file__)
    module_path = os.path.dirname(os.path.dirname(script_path))
    parent_path = os.path.dirname(os.path.dirname(module_path))

    sys.path.append(module_path)
    from python.climelioth import Context, simulate
    from python.epw import epw

    sys.path.append(os.path.dirname(module_path) + "/weather-import/python/")
    import weatherimport
    
    # Parse arguments
    geometry = ''
    std_hyps = ''
    weather = ''
    ratiopath = ''
    programme_file_path = ''
    STDpath = ''
    output_dir = ''
    
    # sample inputs
    std_hyps = os.path.join(parent_path,"testing","consommation","besoins","climelioth","in","1")
    geometry = os.path.join(parent_path,"testing","consommation","geometrie","out","3c3dabc2-ca9f-11ea-b42c-04ea56a22c94","buildings.json")
    weather = os.path.join(parent_path,"testing","meteo","out","H1c","H1c.csv")

    try:
        opts, args = getopt.getopt(argv, "g:s:w:r:p:t:o:", ["geometry=", "std_hyps=", "weather=", "ratios=", "programme=", "STDreplacement=","output="])
    except getopt.GetoptError:
        print('run.py -g <geometry_file_path> -s <std_hyps_file_path> -w <optional_weather_file_path> -o <output_file_path>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-g", "--geometry_file_path"):
            geometry = arg
        elif opt in ("-s", "--std_hyps_file_path"):
            std_hyps = arg
        elif opt in ("-w", "--weather_file_path-or-zone"):
            weather = arg
        elif opt in ("-r", "--ratios_file_path"):
            ratiopath = arg
        elif opt in ("-p", "--programme"):
            programme_file_path = arg
        elif opt in ("-t", "--STDreplacement"):
            STDpath = arg
        elif opt in ("-o", "--output_file_path"):
            output = arg
            output_dir = os.path.dirname(output)

    if programme_file_path == "":
        warnings.warn("Running without scaling areas to programme")
    else:
        programme_areas = pd.read_excel(programme_file_path, sheet_name = 'floor_area')

    # Run climelioth
    # Prepare weather data
    if weather[-4:] == ".csv":
        climeliothwea = pd.read_csv(weather,comment='#')
    elif weather[-4:] == ".epw":
        climeliothwea = weatherimport.getEPW(weather)
    elif weather == "":
        climeliothwea = weatherimport.getRT2012()
    else:
        climeliothwea = weatherimport.getRT2012(weather)

    delta_t = 3600
    context = Context(delta_t=delta_t)
    
    timebase = pd.date_range(start=pd.to_datetime('2005-01-01 00:00:00', format='%Y-%m-%d %H:%M:%S'),
                            end=pd.to_datetime('2005-12-31 23:00:00', format='%Y-%m-%d %H:%M:%S'),
                            freq='H')
    
    context.set_weather(air_temperature = climeliothwea["air_temperature"].values,
                    water_temperature = climeliothwea["water_temperature"].values,
                    sky_temperature = climeliothwea["sky_temperature"].values,
                    air_humidity = climeliothwea["air_humidity"].values,
                    direct_normal_irradiation = climeliothwea["direct_normal_irradiation"].values,
                    diffuse_horizontal_irradiation = climeliothwea["diffuse_horizontal_irradiation"].values,
                    solar_elevation = climeliothwea["solar_elevation"].values,
                    solar_azimuth = climeliothwea["solar_azimuth"].values,
                    wind_speed = climeliothwea["wind_speed"].values)
 
    # Read geometry
    with open(geometry, 'r') as f:
        buildings = json.load(f)

    # Compute power time series
    buildings_df = []

    # Uncomment the line below for fast debugging (only one building simulated)
    #buildings = [buildings[7]]

    for b in buildings:
        
        print(b['building_id'])

        volumes = b['volumes']
        dfs = []

        for v in volumes:

            zones = v['zones']

            
            for z in zones:
                uses = z['uses']
                    
                for u in uses:

                    if u['use'] != "empty":

                        floors = z['floor']

                        for f in floors:
                            with open(os.path.join(output_dir,"irr.txt"),"a") as file:
                                file.write("\n{},{},{}\n".format(b['building_id'],v['volume_id'],z['level']))
                            
                            # TO DO : Test if default parameters should be overriden
                            parameters_file_path = os.path.join(std_hyps, b['building_id'], u['use'], "parameters.json")
                            parameters_file_exists = os.path.isfile(parameters_file_path)
                            
                            if parameters_file_exists is False:
                                parameters_file_path = os.path.join(module_path, 'data', 'hyp', 'buildings', u['use'], 'parameters.json')
                                    
                            # TO DO : Test if default schedules should be overriden
                            schedules_file_path = os.path.join(std_hyps, b['building_id'], u['use'], "schedules")
                            schedules_file_exists = os.path.isdir(schedules_file_path)
                            
                            if schedules_file_exists is False:
                                schedules_file_path = os.path.join(module_path, 'data', 'hyp', 'buildings', u['use'], 'schedules')
                            
                            df = simulate(context = context,
                                         delta_t = 3600,
                                         floor_part = f,
                                         use = u['use'],
                                         use_share = u['share'],
                                         timebase = timebase,
                                         parameters_file_path = parameters_file_path,
                                         schedules_file_path = schedules_file_path,
                                         output_dir = output_dir)
                            
                            df['volume_id'] = v['volume_id']
                            df['level'] = z['level']
                            df['floor_part'] = f['floor_part']
                            
                            dfs.append(df)

        # Add the other loads of the building
        other_loads_file_path = os.path.join(std_hyps, b['building_id'], "other_loads.csv")
        other_loads_file_path_exists = os.path.isfile(other_loads_file_path)
        other_loads = pd.DataFrame()

        if other_loads_file_path_exists:
            other_loads = pd.read_csv(other_loads_file_path, header=0)

            loads_df = df.copy()
            loads_df.iloc[:, 3:] = 0
            loads_df.loc[:, "floor_area"] = 0
            loads_df.loc[:, "use"] = "process"
            loads_df['p_heat'] = other_loads.loc[:,"p_heat"].values*1000
            loads_df['p_cool'] = -other_loads.loc[:,"p_cool"].values*1000
            loads_df['p_equi'] = other_loads.loc[:,"p_elec"].values*1000
            
            dfs.append(loads_df)
        
        if (len(dfs) > 0):
            dfs = pd.concat(dfs)
            dfs['building_id'] = b['building_id']
            buildings_df.append(dfs)

    buildings_df = pd.concat(buildings_df)
    
    # --------------------------
    # Compute the model areas for comparison with the programme
    power = buildings_df
    areas = power[power['timebase'] == power['timebase'].unique()[0]]
    areas = areas.groupby(['building_id', 'use'], as_index=False)['floor_area'].sum()
    areas.columns = ['building_id', 'use', 'climelioth_floor_area']
    print(areas)
    if len(programme_file_path) > 0:


        programme_areas = programme_areas.groupby(['building_id', 'climelioth_use'], as_index=False)['floor_area'].sum()

        # Compute the hourly power demand at building and use level
        power = power.groupby(['building_id', 'timebase', 'use', "system_heating", "system_cooling"], as_index=False).sum()
        
        # Compute the power correction coefficient
        # to correct SIG geometry data and match the programme areas
        programme_areas = pd.merge(
            areas, programme_areas,
            right_on = ['building_id', 'climelioth_use'],
            left_on = ['building_id', 'use'],
            how = 'left'
        )
        
        # Create a dummy floor area for process loads which have no floor area in the model (to avoid dividing by zero)
        programme_areas.loc[programme_areas['use'] == 'process', 'floor_area'] = 1.0
        programme_areas.loc[programme_areas['use'] == 'process', 'climelioth_floor_area'] = 1.0
        
        programme_areas['k'] = programme_areas['floor_area']/programme_areas['climelioth_floor_area']

        print("Scaling factors climelioth - programme :")
        print(programme_areas)
        
        power = pd.merge(power, programme_areas[['building_id', 'use', 'k']], on=["building_id", "use"])

        # Apply the power correction
        cols = ['floor_area', 'p_vent', 'p_light', 'p_equi', 'p_heat', 'p_heat_reco', 'p_cool', 'p_cool_reco', "p_cool_process", "p_hw", "p_aux", "hw_volume"]
        for c in cols:
            power[c] = power[c]*power['k']
            

    else:
        power = pd.merge(power, areas, on=["building_id", "use"])
        power = power.rename({"climelioth_floor_area":"floor_area"},axis=1)
    
    buildings_df = power
    print(power.groupby(["building_id"]).sum())
    # --------------------------
        
    def addAuxiliary(buildings_df):
        warnings.warn("Adding auxiliary electrical consumption: this is a beta feature")
        # Auxiliary electrical consumption for heating and cooling
        # Water
        buildings_df["water_flow"] = 0
        buildings_df["deltaT"]  = 0
        #Heating
        buildings_df.loc[buildings_df["system_heating"] == "radiators","deltaT"] = 40
        buildings_df.loc[buildings_df["system_heating"] == "convective","deltaT"]= 40
        buildings_df.loc[buildings_df["system_heating"] == "radiant_ceiling","deltaT"] = 10
        buildings_df.loc[:,"water_flow"] += buildings_df["p_heat"] / (1160  * buildings_df["deltaT"]) # W / (Wh/(m3*K) * K) = m3/h
        
        #cooling
        buildings_df.loc[buildings_df["system_cooling"] == "convective", "deltaT"] = 40
        buildings_df.loc[buildings_df["system_cooling"] == "radiant_ceiling","deltaT"] = 10
        buildings_df["water_flow"] += buildings_df["p_cool"].abs() / (1160  * buildings_df["deltaT"]) # W / (Wh/(m3*K) * K) = m3/h
        
        #dhw
        buildings_df["water_flow"] += buildings_df["hw_volume"] 
        
        pump_cons = 4000 #W/m3/h #https://www.researchgate.net/profile/Bryan_Karney/publication/263927622/figure/fig2/AS:296615354093574@1447730019808/Sample-testing-results-for-100-hp-75-kW-pump-power-consumption_W640.jpg
        buildings_df["p_aux"] = pump_cons * buildings_df["water_flow"] #W/m3 * m3 = W
        
        building_df = buildings_df.drop(["water_flow","deltaT"], axis = 1)
        return buildings_df
    
    buildings_df = addAuxiliary(buildings_df)

    # Apply ratios
    if os.path.basename(ratiopath) != '.csv' and len(ratiopath) > 0:
        buildings_df.drop(["system_heating", "system_cooling"],axis=1).to_parquet(output.replace(".parquet","_noRatios.parquet"))
        ope = pd.read_csv(ratiopath)
        buildings_df = ratios.scale(buildings_df,ope)
    
    # Sum power and floor_area at building / use level
    buildings_df = buildings_df[['building_id', "use", "level","floor_area", "timebase", 'p_vent', 'p_light', 'p_equi', 'p_heat', 'p_heat_reco', "p_hw", 'p_cool', 'p_cool_reco', "p_cool_process", "p_aux", "system_heating", "system_cooling", "hw_volume"]]
    buildings_df = pd.melt(buildings_df, id_vars=['building_id', "level", "use", "timebase", "system_heating", "system_cooling"])
    floor_area = buildings_df.groupby(["building_id", "use", "level", "timebase","variable","system_heating", "system_cooling"], as_index=False)["value"].first()
    buildings_df = buildings_df.groupby(["building_id", "use","timebase", "variable","system_heating", "system_cooling"], as_index=False)["value"].sum()
    buildings_df = pd.pivot_table(buildings_df, index=['building_id', "use", "timebase", "system_heating", "system_cooling"], columns="variable", values="value")
    floor_area = pd.pivot_table(floor_area, index=['building_id', "use", "level","timebase", "system_heating", "system_cooling"], columns="variable", values="value")
    buildings_df["floor_area"] = floor_area.groupby(["building_id", "use","level","timebase","system_heating", "system_cooling"]).first().reset_index("level").groupby(["building_id", "use","timebase","system_heating", "system_cooling"]).sum()["floor_area"]
    buildings_df.reset_index(inplace=True)


    # Replace STD
    if len(STDpath) > 0:
        buildings_df.drop(["system_heating", "system_cooling", "hw_volume"],axis=1).to_parquet(output.replace(".parquet", "_noSTD.parquet"))
        buildings_df = replaceSTD.do(buildings_df, STDpath)
        buildings_df = addAuxiliary(buildings_df) #Recalculate auxiliary consumption
    

    # Write result to file
    buildings_df.drop(["system_heating", "system_cooling","hw_volume"],axis=1).to_parquet(output)


if __name__ == "__main__":
    main(sys.argv[1:])