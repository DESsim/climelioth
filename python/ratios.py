# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 09:22:18 2019

@author: G.Peronato
"""
import pandas as pd
import numpy as np

def getNorm(climelioth):
    cumulative = climelioth.groupby(["building_id","volume_id","use"]).sum()/1000
    floor_area_levels = climelioth.groupby(["building_id","volume_id","use","level"]).first()["floor_area"].unstack()
    floor_area = floor_area_levels.sum(axis=1)
    normalized = cumulative.div(floor_area, axis=0)
    normalized["floor_area"] = floor_area
    
    normalized = normalized.reset_index(level=[0,1,2])
    return normalized

def scale(climelioth, ope):
    scaled = climelioth.copy()
    normalized = getNorm(climelioth)
    print("Scaling power to external ratio")
    #scaled["p_heat"] = climelioth.apply(lambda x: x.p_heat * (ope.loc[ope.use == x.use,"Heating"] / normalized.loc[(normalized.building_id == x.building_id) & (normalized.use == x.use),"p_heat"]), axis=1)
    for building in scaled.building_id.unique():
        for volume in scaled[(scaled.building_id == building)].volume_id.unique():
            for use in scaled[(scaled.building_id == building) & (scaled.volume_id == volume)].use.unique():
                if use != "process":
                    for source in ope.columns[1:]:
                        if source == "heat" or source == "cool":
                            gross = normalized.loc[(normalized.building_id == building)& (normalized.volume_id == volume) & (normalized.use == use),"p_"+source].values[0]
                            reco = normalized.loc[(normalized.building_id == building)& (normalized.volume_id == volume) & (normalized.use == use),"p_"+source+"_reco"].values[0]
                            simulated = gross - reco
                        else:
                            simulated = normalized.loc[(normalized.building_id == building)& (normalized.volume_id == volume) & (normalized.use == use),"p_"+source].values[0]
                        ratio  = ope.loc[ope.use == use,source].values[0]
                        scalingfactor =  abs(ratio / simulated)

                        if simulated > 0.0:

                            if simulated < 0.33*ratio:
                                print("Scaling",building,use,source)
                                k = 0.33*scalingfactor
                                scaled.loc[(scaled.building_id == building) & (scaled.volume_id == volume) & (scaled.use == use),"p_"+source] *= k
                                if source == "heat" or source == "cool":
                                    scaled.loc[(scaled.building_id == building) & (scaled.volume_id == volume) & (scaled.use == use),"p_"+source+"_reco"] *= k

                            elif simulated > 1.66*ratio:
                                print("Scaling",building,use,source)
                                k = 1.66*scalingfactor
                                scaled.loc[(scaled.building_id == building) & (scaled.volume_id == volume) & (scaled.use == use),"p_"+source] *= k
                                if source == "heat" or source == "cool":
                                    scaled.loc[(scaled.building_id == building) & (scaled.volume_id == volume) & (scaled.use == use),"p_"+source+"_reco"] *= k
    
    return scaled

if __name__ == "__main__":
    climeliothpath = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\climelioth\out\{}\climelioth.parquet"
    scenario = "284d0e14-ffd8-11e9-99ce-d4e099056fc8"
    climeliothpath = climeliothpath.format(scenario)
    climelioth = pd.read_parquet(climeliothpath)
    
    climelioth["p_heat"] -= climelioth["p_heat_reco"]
    climelioth["p_cool"] -= climelioth["p_cool_reco"]
    #climelioth = climelioth.drop(["p_heat_reco","p_cool_reco"],axis=1)
    
#    cumulative = climelioth.groupby(["building_id","use"]).sum()/1000
#    floor_area_levels = climelioth.groupby(["building_id","volume_id","use","level"]).first()["floor_area"].unstack()
#    floor_area = floor_area_levels.sum(axis=1).reset_index()
#    floor_area = floor_area.groupby(["building_id","use"])[0].sum()
#    normalized = cumulative.div(floor_area, axis=0)
#    normalized["floor_area"] = floor_area
#    normalized = normalized.reset_index(level=[0,1])
    
    normalized = getNorm(climelioth)
    print(normalized[normalized.use != "process"].groupby("use").median()[["p_heat","p_cool","p_light","p_hw"]].round(1).abs())
    
    print(normalized[normalized.use != "process"].groupby(["building_id","use"]).median()[["p_heat","p_cool","p_light","p_hw"]].round(1).abs())
#    ope = pd.read_csv(r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\ratios\bruneseau_p50.csv")
#    
#    scaled = scale(climelioth,ope)
#    normscaled = getNorm(scaled)
