set PYTHONPATH=%PYTHONPATH%;D:\DATA\F.POUCHAIN\dev\workflow\python

luigi --module workflow RunClimelioth --geometry-version  --std-hyps-version tests --weather-input Paris_Montsouris-hour.epw

luigi --module workflow RunClimelioth --geometry-version 5 --std-hyps-version allequal-WWR0 --weather Paris_Montsouris-hour.epw --ratios ope_H1a_2017_p10

luigi --module workflow RunClimelioth --geometry-version check_masks --std-hyps-version tests --weather-input Paris_Montsouris-hour.epw